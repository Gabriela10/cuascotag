//
//  ViewController.swift
//  cuascotaG
//
//  Created by Gabriela Cuascota on 5/6/18.
//  Copyright © 2018 Gabriela Cuascota. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var nameTextFile: UITextField!
    
    var name: Name?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func NextButtonPressed(_ sender: Any) {
    
        let itemName = nameTextFile.text ?? ""
        
        let item = Item(name: itemName)
        
        if item.name == " " {
            showAlert(name: "ERROR", message: "Name is required")
        }
        Name?.HiItems += [item]
        navigationController?.popViewController(animated: true)
    }
    
    func showAlert(name:String, message: String){
        let alert = UIAlertController(title: name, message:message, preferredStyle: .alert)
        let okAction = UIAlertAction (title: "OK", style: .default, handler: nil)
        
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
}
}
