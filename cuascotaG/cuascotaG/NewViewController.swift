//
//  NewViewController.swift
//  cuascotaG
//
//  Created by Gabriela Cuascota on 5/6/18.
//  Copyright © 2018 Gabriela Cuascota. All rights reserved.
//

import UIKit

class NewViewController: UIViewController, UITableViewDelegate {

    let name = Name()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var hiLabel: UILabel!
    
    @IBOutlet weak var itemsTableView: UITableView!
    
    @IBOutlet weak var averageLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        itemsTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toNewInfoSegue" {
            let destination = segue.destination as! NewViewController
            destination.name = name
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return name.toDoItems.count
        }
        return name.doneItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell") as! itemTableViewCell
        
        if indexPath.section == 0 {
            cell.nameLabel.text = name.toDoItems[indexPath.row].name
            
        }else{
            cell.nameLabel.text = name.doneItems[indexPath.row].name
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            performSegue(withIdentifier: "toNewInfoSegue", sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "Hi": "Valor"
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            name.checkItem(index: indexPath.row)
        }else{
            name.unCheckItem(index: indexPath.row)
        }
        itemsTableView.reloadData()
    }

}
